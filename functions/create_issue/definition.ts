// functions/create_issue/definition.ts

import { DefineFunction, Schema } from "deno-slack-sdk/mod.ts";

const CreateIssueDefinition = DefineFunction({
  callback_id: "create_issue",
  title: "Create GitLab issue",
  description: "Create a new GitLab issue in a repository",
  source_file: "functions/create_issue/mod.ts",
  output_parameters: {
    properties: {
      GitLabIssueNumber: {
        type: Schema.types.number,
        description: "Issue number",
      },
      GitLabIssueLink: {
        type: Schema.types.string,
        description: "Issue link",
      },
    },
    required: ["GitLabIssueNumber", "GitLabIssueLink"],
  },
});

export default CreateIssueDefinition;
