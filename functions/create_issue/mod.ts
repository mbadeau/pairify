import { SlackFunction } from "deno-slack-sdk/mod.ts";
import CreateIssueDefinition from "./definition.ts";

// https://docs.github.com/en/rest/issues/issues#create-an-issue
export default SlackFunction(
  CreateIssueDefinition,
  async ({ inputs, env }) => {
    const headers = {
      "PRIVATE-TOKEN": env.GITLAB_TOKEN,
      "Content-Type": "application/json",
    };

    const { title, description } = inputs;

    try {
      const hostname = env.GITLAB_DOMAIN;
      const projectId = env.GITLAB_PROJECT_ID;

      // https://docs.github.com/en/enterprise-server@3.3/rest/guides/getting-started-with-the-rest-api
      const apiEndpoint = new URL(
        `https://${hostname}/api/v4/projects/${projectId}/issues`,
      );

      const body = JSON.stringify({
        // title: title,
        // description: description,
        title: "testing",
        description: "testing",
      });

      console.log(body);

      const issue = await fetch(apiEndpoint, {
        method: "POST",
        headers,
        body,
      }).then((res: Response) => {
        if (res.status === 201) return res.json();
        else throw new Error(`${res.status}: ${res.statusText}`);
      });

      return {
        outputs: {
          GitLabIssueNumber: issue.iid,
          GitLabIssueLink: issue.web_url,
        },
      };
    } catch (err) {
      console.error(err);
      return {
        error:
          `An error was encountered during issue creation: \`${err.message}\``,
      };
    }
  },
);
