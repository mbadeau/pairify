import { DefineWorkflow, Schema } from "deno-slack-sdk/mod.ts";
import CreateIssueDefinition from "../functions/create_issue/definition.ts";

const CreateNewIssueWorkflow = DefineWorkflow({
  callback_id: "create_new_issue_workflow",
  title: "Create new issue",
  description: "Create a new GitLab issue",
  input_parameters: {
    properties: {
      interactivity: {
        type: Schema.slack.types.interactivity,
      },
      channel: {
        type: Schema.slack.types.channel_id,
      },
    },
    required: ["interactivity", "channel"],
  },
});

const issue = CreateNewIssueWorkflow.addStep(CreateIssueDefinition, {
  title: "test",
  description: "test",
});

CreateNewIssueWorkflow.addStep(Schema.slack.functions.SendMessage, {
  channel_id: CreateNewIssueWorkflow.inputs.channel,
  message:
    `Issue #${issue.outputs.GitLabIssueNumber} has been successfully created\n` +
    `Link to issue: ${issue.outputs.GitLabIssueLink}`,
});

export default CreateNewIssueWorkflow;
