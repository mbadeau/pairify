import { Manifest } from "deno-slack-sdk/mod.ts";
import CreateIssueDefinition from "./functions/create_issue/definition.ts";
import CreateNewIssueWorkflow from "./workflows/create_new_issue.ts";

/**
 * The app manifest contains the app's configuration. This
 * file defines attributes like app name and description.
 * https://api.slack.com/future/manifest
 */
export default Manifest({
  name: "Workflows for GitLab",
  description: "Bringing oft-used GitLab functionality into Slack",
  icon: "assets/icon.png",
  functions: [CreateIssueDefinition],
  workflows: [CreateNewIssueWorkflow],
  outgoingDomains: ["gitlab.antons.io", "gitlab.com"],
  botScopes: ["commands", "chat:write", "chat:write.public"],
});
